eyelow: Talk to the HP Integrated Lights-Out console
====================================================

:Author: William ML Leslie
:License: MIT License (Expat)

Connecting to the system console of a computer running the first
version of Hewlet Packard's iLO Baseboard Management Controller is a
rigmarole in 2020.  You need to install an old browser that will allow
you to get around hostname verification, and you'll need the outdated
and insecure proprietary junk that is the Java Browser Plugin.  You'll
also need a Java 6 install, which it seems these days requires getting
an oracle account.

If you go that route, I recommend Firefox 20.  If you have a 64 bit
java 6 install, you can use the 64-bit browser with the plugin, no
matter what the internet says about plugins not being supported on the
64-bit version of firefox.  You can then place a symlink to
``jre/lib/amd64/libnpjp2.so`` into ``~/.mozilla/plugins`` and login to
the iLO portal.  I recommend using a standalone VM for this that you
will never use to browse the web.  By the way, I think the iLO cards
will only connect to 192.168 subnets, as I could not connect via my
10.0 network at home.

This project is an attempt to talk to the remote console without all
of the proprietary gunk that never should have been required in the
first place.  It fetches the console parameters over unverified https,
connects to the required telnet port, and then attempts to speak the
proprietary "encryption" extension.  It turns out this extension uses
ARC4 to communicate, and frames messages with a length and md5 hash to
validate the decryption.

It turns out I have not been successful so far in talking this
protocol - the card does not respond to the encrypted sessionid
message.  I need to look into the framing to make sure I'm not missing
something obvious.  That said, if you wanted to start talking to a
first-generation iLO BMC, such as on the wonderful RX 2620, this
project might be a good place to start.

On Twisted
----------

This project has turned out more of an indictment of my favourite web
framework than anything.

There are things that I *know* are not great about Twisted.  For
example, Perspective Broker is a pale, sad synchronous RPC protocol in
an otherwise asynchronous framework, and it isn't like
promise-pipelining RPC protocols, such as CapTP, do not exist.  That
said, most of the world makes this mistake - consider GraphQL or gRPC.
Similarly, I'm aware that when using Twisted's Deferreds using the
provided API, you're bound to hit issues with stale stack-frames.
This is one area where the real world has caught up, for example, this
is not a problem in Javascript's `Promises/A+`_ as the ``onFulfilled``
and ``onRejected`` handlers run in a new turn.

.. _`Promises/A+`: https://promisesaplus.com

However, browsing the source will demonstrate that there are some
serious flaws in the provided protocols.  Take for example
`eyelow.web`, which does in 192 lines what requests would have done
in fewer than 20.  Notice there that I had to write my own body
handler just to pass form-encoded data.  Or
`eyelow.telnet.EncryptedTelnetTransport.dataReceived`, which contains
a wholesale copy of the protocol implementation in
`twisted.conch.telnet.TelnetTransport`.  Why? Because the encryption
extension changes the meaning of the data that has already been read;
however, the existing implementation only keeps this data in a local
variable.

License
-------

Copyright (c) 2019, 2020 William ML Leslie

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
