# Copyright (c) 2019, 2020 William ML Leslie

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import print_function

import urllib.parse
import xml.etree.ElementTree

from zope.interface import implementer

from twisted.internet.defer import inlineCallbacks, Deferred, returnValue
from twisted.internet._sslverify import ClientTLSOptions
from twisted.python import compat
from twisted.web.client import Agent
from twisted.web.iweb import IPolicyForHTTPS
from twisted.web.client import (Agent, ResponseFailed,
                                CookieAgent, BrowserLikePolicyForHTTPS)
from twisted.web.client import _ReadBodyProtocol, ResponseDone
from twisted.web.iweb import IBodyProducer
from twisted.web.http import PotentialDataLoss
from twisted.web.http_headers import Headers
from OpenSSL.SSL import Context, TLSv1_METHOD


class SSLv2ClientContextFactory:
    """A context factory for SSL clients that can talk to an iLO MP."""

    isClient = 1

    # iLO 1 speaks a truly ancient SSL version; we'll enable that
    # here.
    method = TLSv1_METHOD

    _contextFactory = Context

    def getContext(self):
        return self._contextFactory(self.method)


class NonVerifyingClientTLSOptions(ClientTLSOptions):
    def _identityVerifyingInfoCallback(self, connection, where, ret):
        """Avoid verifying hostnames.

        We don't have a DNS server on this network, so do nothing.
        """
        pass


@implementer(IPolicyForHTTPS)
class ILOWorkaroundPolicy(object):
    def __init__(self):
        self._normalPolicy = SSLv2ClientContextFactory()

    def creatorForNetloc(self, hostname, port):
        return NonVerifyingClientTLSOptions(hostname.decode('ascii'),
                                            self._normalPolicy.getContext())


class TestReadBodyProtocol(_ReadBodyProtocol, object):
    def dataReceived(self, data):
        _ReadBodyProtocol.dataReceived(self, data)

    def connectionLost(self, reason):
        """
        Deliver the accumulated response bytes to the waiting L{Deferred}, if
        the response body has been completely received without error.
        """
        if reason.check(ResponseDone) or reason.check(PotentialDataLoss):
            self.deferred.callback(b''.join(self.dataBuffer))
        else:
            super(TestReadBodyProtocol, self).connectionLost(reason)


def readBody(response):
    d = Deferred()
    response.deliverBody(
        TestReadBodyProtocol(response.code, response.phrase, d)
    )
    return d


@implementer(IBodyProducer)
class FormBodyProducer(object):
    def __init__(self, fields):
        self.data = urllib.parse.urlencode(fields).encode('ascii')
        self.length = len(self.data)

    def startProducing(self, consumer):
        d = Deferred()
        consumer.write(self.data)
        d.callback(None)
        return d

    def pauseProducing(self):
        pass # no.

    def resumeProducing(self):
        pass # no.

    def stopProducing(self):
        pass # no.


def get_console_parameters(reactor, username, password, hostname, pool=None):
    cookie_jar = compat.cookielib.CookieJar()
    agent = CookieAgent(
        Agent(reactor, ILOWorkaroundPolicy(), pool=pool),
        cookie_jar,
    )
    address = 'https://%s/' % hostname
    headers = Headers({
        # 'User-Agent': ['twisted-python']
        'Host': [hostname],
        'Accept': ['text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'],
        'Accept-Language': ['en-AU'],
        'Accept-Encoding': ['gzip, deflate, br'],
        'Referer': [address],
        'User-Agent': ['Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0'],
    })
    def request(path, data=None):
        method = b'GET' if data is None else b'POST'
        d = agent.request(method, address.encode('ascii') + path, headers,
                          data)
        @d.addCallback
        def cbResponse(response):
            if response.code == 200:
                return readBody(response)
            raise Exception("Failed: %r, %r" %
                            (response.code, response.phrase))
        return d

    @inlineCallbacks
    def console_parameter_process():
        body = yield request(b'')
        body = yield request(
            b'signin.html',
            FormBodyProducer({'loginId': username, 'password': password}),
        )
        if not any(cookie.name == 'MPID' for cookie in cookie_jar):
            print("Failed to log-in:")
            print(body)
            returnValue({})
            return
        body = yield request(b'remoteConsole/console.html')
        config = parse_parameters(body)
        if not config:
            print(cookie_jar)
            print("Failed to get parameters:")
            print(body)
        returnValue(config)
    return console_parameter_process()


def parse_parameters(body):
    console = xml.etree.ElementTree.fromstring(body)
    params = {}
    for element in console.findall('.//{http://www.w3.org/1999/xhtml}param'):
        params[element.attrib["name"]] = element.attrib["value"]
    return params


if __name__ == '__main__':
    from twisted.internet import reactor
    from getpass import getpass
    password = getpass("enter password: ")
    d = get_console_parameters(reactor, 'Admin', password,
                               hostname='192.168.15.100')

    @d.addCallback
    def stop(data):
        print("got console configuration:")
        for key, value in sorted(data.items()):
            print(key.rjust(15), ":", value)
        reactor.stop()

    @d.addErrback
    def report_and_stop(failure):
        print("=" * 80)
        print(failure)
        print(repr(failure))
        failure.printTraceback()
        reactor.stop()
    reactor.run()
