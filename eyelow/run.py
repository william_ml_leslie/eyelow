# Copyright (c) 2020 William ML Leslie

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os, tty, sys, termios
from getpass import getpass
from twisted.internet import reactor
from twisted.internet.defer import inlineCallbacks, Deferred


from eyelow.web import get_console_parameters
from eyelow.console import connect_telnet

CONSOLE_PARAMETERS = {
    'Host': '192.168.15.101',
    'Port': '2023',
    'SessionID': '00000070',
    'Param1': 'f64648f16ef6ff4bafcca7c493c665d9',
    'Param2': '6866d201c5206d6924b0938710577b05',
    'Param3': '2a3722299ef018a931929ec8f85c3165',
    'Param4': '155a78bb2f09561d0edd005b86539f60',
}

CONSOLE_PARAMETERS = None

def main(username='Admin', password=None):
    fd = sys.__stdin__.fileno()
    oldSettings = termios.tcgetattr(fd)

    if not CONSOLE_PARAMETERS:
        if password is None:
            password = getpass(
                "Please enter the iLO password for %r " % username
            )
        d = get_console_parameters(reactor, username, password,
                                   hostname="192.168.15.100")
    else:
        d = Deferred()
        d.callback(CONSOLE_PARAMETERS)

    def got_config(data):
        if not data:
            print("Authentication Failed")
            reactor.stop()
            return
        print("got console configuration:")
        for key, value in sorted(data.items()):
            print(key.rjust(15), ":", value)
        print("preparing to connect...")
        reactor.callLater(0.5, telnet, data)

    @inlineCallbacks
    def telnet(data):
        print("connecting...")
        try:
            endpoint = yield connect_telnet(reactor, data)
        except Exception:
            termios.tcsetattr(fd, termios.TCSANOW, oldSettings)
            os.write(fd, b"\r\x1bc\r")
            #tty.setraw(fd)
            import traceback
            traceback.print_exc()
            reactor.stop()
        else:
            print("connected")

    def stop(ignored):
        reactor.stop()

    def report_and_stop(failure):
        print("=" * 80)
        print(failure)
        print(repr(failure))
        failure.printTraceback()
        reactor.stop()

    d.addCallbacks(got_config, report_and_stop)
    reactor.run()

if __name__ == '__main__':
    main()
