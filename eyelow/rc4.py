# Copyright (c) 2019, 2020 William ML Leslie

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

def keystream(key):
    key = bytearray(key)
    subs = list(range(256))
    j = 0
    for i in range(256):
        key_byte = key[i % len(key)]
        j = (j + subs[i] + key_byte) & 0xff
        subs[i], subs[j] = subs[j], subs[i]

    i = j = 0
    while True:
        i = (i + 1) & 0xff
        j = (j + subs[i]) & 0xff
        subs[i], subs[j] = subs[j], subs[i]
        yield subs[(subs[i] + subs[j]) & 0xff]


class ARC4(object):
    def __init__(self, key):
        self.keystream = keystream(key)

    def crypt(self, data):
        return bytearray(d ^ b for d, b in zip(data, self.keystream))

