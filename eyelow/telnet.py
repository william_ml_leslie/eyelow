# Copyright (c) 2019, 2020 William ML Leslie and Twisted Matrix Laboratories

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import print_function

from twisted.internet.protocol import Factory, Protocol
from twisted.internet import stdio, reactor
from twisted.conch.telnet import TelnetProtocol, TelnetTransport
from twisted.conch.telnet import (IAC, SB, SE, NOP, DM, BRK, IP, AO, AYT,
                                  EC, EL, GA, WILL, WONT, DO, DONT,
                                  LINEMODE_SLC)
from eyelow.rc4 import ARC4
import hmac
import hashlib
import struct

MD5 = hashlib.md5()
TC_APP_MSG = b'\x2c' # 44
TC_ENCRYPT = b'\x26' # 38


class EncryptedTelnetFactory(Factory):
    def __init__(self, options=()):
        self.terminal = None
        self.transport = None
        self.opt = {
            'session_id': options.get('SessionID', '00000000'),
            'read_rc4': options.get('Param2', ''),
            'write_rc4': options.get('Param4', ''),
            'read_md5': options.get('Param3', ''),
            'write_md5': options.get('Param1', ''),
        }
        self.opt.update(options)

    def buildProtocol(self, addr):
        self.terminal = TelnetTerminal()
        stdio.StandardIO(self.terminal)
        #self.terminal.registerProducer(self.stdio)
        self.transport = EncryptedTelnetTransport(self.terminal, **self.opt)
        self.transport.factory = self
        return self.transport


class EncryptedTelnetTransport(TelnetTransport, object):
    message_buffer = None

    def __init__(self, terminal, session_id, read_rc4, write_rc4,
                 read_md5, write_md5, **kwargs):
        super(EncryptedTelnetTransport, self).__init__()
        self.terminal = terminal
        # self.commandMap.update({
        #     TC_APP_MSG: self.telnet_APP_MSG,
        #     TC_ENCRYPT: self.telnet_ENCRYPT,
        # })
        self.negotiationMap.update({
            TC_APP_MSG: self.negotiate_APP_MESSAGE,
            TC_ENCRYPT: self.negotiate_ENCRYPT,
        })
        self.session_id = session_id.encode('ascii')
        self.readRC4 = ARC4(bytes.fromhex(read_rc4))
        self.writeRC4 = ARC4(bytes.fromhex(write_rc4))
        self.readMD5 = hashlib.md5(bytes.fromhex(read_md5))
        self.writeMD5 = hashlib.md5(bytes.fromhex(write_md5))
        self.data = ''
        self.offset = 0
        self.read_encrypted = False
        self.write_encrypted = False

    def connectionMade(self):
        self.data = ''
        self.offset = 0
        self.log = open('telnet.log', 'a')
        self.log.write('connectionMade\n')
        super(EncryptedTelnetTransport, self).connectionMade()

        self.transport.write(b''.join([
            IAC, SB,
            # packet inspection reveals the length is not provided.
            b'\x2a', self.session_id, # 42
            IAC, SE
        ]))
        self.terminal.registerTelnet(self)

    def connectionLost(self, reason):
        self.data = ''
        self.offset = 0
        self.terminal.registerTelnet(None)
        self.log.close()

    def negotiate(self, data):
        command = data[0]
        if command not in self.negotiationMap:
            print('no negotiation?:', repr(command),
                  self.negotiationMap.keys())
        super(EncryptedTelnetTransport, self).negotiate(data)

    def unhandledSubnegotiation(self, command, data):
        print('unhandled:', repr(command), ord(command), repr(data))
    
    def negotiate_ENCRYPT(self, rest):
        if rest[0] == b'\x2e': # 46
            print('negotiating_begin_encryption')
            self.log.write('negotiating_begin_encryption\n')
            self.read_encrypted = True
            self.data = self.readRC4.crypt(self.data[offset:])
            self.offset = 0
        elif rest[0] == b'\x2f': # 47
            print('negotiating_request_encrypted')
            self.log.write('negotiating_request_encrypted\n')
            self.write_encrypted = True
            self._write(b''.join([
                IAC, SB,
                b'\x2b', self.session_id, # 43
                IAC, SE
            ]))
            # system then sends -1, 251, 3 (IAC WILL 3?)  original
            # system doesn't buffer, so we'll emulate that here with
            # delay.
            self.will(LINEMODE_SLC)
        else:
            print('unhandled negotiate_encrypt:', repr(rest), repr(rest[0]))

    def negotiate_APP_MESSAGE(self, rest):
        error_code = int(b''.join(rest))
        if error_code == -1003:
            self.log.write('error: Session error\n')
            print("Session error")
        elif error_code == -1002:
            self.log.write('error: Session id error\n')
            print("Session id error")
        elif error_code == -1001:
            self.log.write('error: Encryption error\n')
            print("Encryption error")
        else:
            self.log.write('error: %r\n' % self.data)
            print("Host error: %r" % self.data)

    def decrypt(self, data):
        result = []
        while self.offset < len(self.data):
            b = self.data[self.offset]
            self.offset += 1
            if self.decrypt_state == 'buf_width':
                self.buf_width_bytes.append(b)
                if len(self.buf_width_bytes) == 4:
                    self.buf_width, = struct.unpack('<I',
                        b''.join(self.buf_width_bytes))
                    self.buf_width_bytes = []
                    self.decrypt_state = 'MAC'
                continue
            if self.decrypt_state == 'MAC':
                self.mac_bytes.append(b)
                if len(self.mac_bytes) == MD5.digest_size:
                    self.given_mac = b''.join(self.mac_bytes)
                    self.decrypt_state = 'encrypted-message'
                continue
            if self.decrypt_state == 'encrypted-message':
                end = self.offset + self.buf_width
                if len(self.data) < end:
                    break
                message = self.data[self.offset:end]
                hmac = self.readMD5.copy()
                hmac.update(message)
                digest = hmac.digest()
                if hmac.compare_digest(digest, self.given_mac):
                    print('digests not equal: %r != %r' %
                          (digest, self.given_mac))
                else:
                    result.append(message)
        return b''.join(result)

    def dataReceived(self, data):
        """Clone of twisted.conch.telnet.Telnet.dataReceived.

        The method is not very flexible as it does not allow you to
        add commands or alter the data stream due to subnegotiation,
        so we've copied it here to provide more flexible parsing.

        """
        self.log.write('recieved: %r\n' % data)
        if self.read_encrypted:
            data = self.readRC4.crypt(data)
        self.log.write('decrypted: %r\n' % data)

        self.data = data
        self.offset = 0
        appDataBuffer = bytearray()

        print('input:', self.data)
        while self.offset < len(self.data):
            b = bytes(bytearray([self.data[self.offset]]))
            self.offset += 1
            if self.state == 'data':
                if b == IAC:
                    self.state = 'escaped'
                elif b == '\r':
                    self.state = 'newline'
                else:
                    appDataBuffer.extend(b)
            elif self.state == 'escaped':
                if b == IAC:
                    appDataBuffer.extend(b)
                    self.state = 'data'
                elif b == SB:
                    self.state = 'subnegotiation'
                    self.commands = []
                elif b in (NOP, DM, BRK, IP, AO, AYT, EC, EL, GA):
                    self.state = 'data'
                    if appDataBuffer:
                        self.applicationDataReceived(bytes(appDataBuffer))
                        del appDataBuffer[:]
                    self.commandReceived(b, None)
                elif b in (WILL, WONT, DO, DONT):
                    self.state = 'command'
                    self.command = b
                else:
                    raise ValueError("Stumped", b)
            elif self.state == 'command':
                self.state = 'data'
                command = self.command
                del self.command
                if appDataBuffer:
                    self.applicationDataReceived(bytes(appDataBuffer))
                    del appDataBuffer[:]
                self.commandReceived(command, b)
            elif self.state == 'newline':
                self.state = 'data'
                if b == b'\n':
                    appDataBuffer.extend(b'\n')
                elif b == b'\0':
                    appDataBuffer.extend(b'\r')
                elif b == IAC:
                    # IAC isn't really allowed after \r, according to the
                    # RFC, but handling it this way is less surprising than
                    # delivering the IAC to the app as application data. 
                    # The purpose of the restriction is to allow terminals
                    # to unambiguously interpret the behavior of the CR
                    # after reading only one more byte.  CR LF is supposed
                    # to mean one thing (cursor to next line, first column),
                    # CR NUL another (cursor to first column).  Absent the
                    # NUL, it still makes sense to interpret this as CR and
                    # then apply all the usual interpretation to the IAC.
                    appDataBuffer.extend(b'\r')
                    self.state = 'escaped'
                else:
                    appDataBuffer.extend(b'\r' + b)
            elif self.state == 'subnegotiation':
                if b == IAC:
                    self.state = 'subnegotiation-escaped'
                else:
                    self.commands.append(b)
            elif self.state == 'subnegotiation-escaped':
                if b == SE:
                    self.state = 'data'
                    commands = self.commands
                    del self.commands
                    if appDataBuffer:
                        self.applicationDataReceived(bytes(appDataBuffer))
                        del appDataBuffer[:]
                    #import pdb
                    #pdb.set_trace()
                    self.negotiate(commands)
                else:
                    self.state = 'subnegotiation'
                    self.commands.append(b)
            else:
                raise ValueError("How'd you do this?")

        if appDataBuffer:
            self.applicationDataReceived(bytes(appDataBuffer))


    def encrypt(self, message):
        if not self.write_encrypted:
            return message
        width = struct.pack('<I', len(message))
        hmac = self.writeMD5.copy()
        hmac.update(message)
        digest = hmac.digest()
        assert len(digest) == hmac.digest_size
        print('encrypting:', width, repr(digest), repr(message), len(message))
        data = b''.join([width, digest, message])
        #return self.writeRC4.crypt(data)
        encrypted = self.writeRC4.crypt(data)
        print('sending-encrypted:', repr(encrypted))
        return bytes(encrypted)

    def _write(self, data):
        # Telnet Protocol uses this to write to the underlying transport.
        self.log.write('sent: %r\n' % data)
        self.transport.write(self.encrypt(data))

    def write(self, data):
        # ProtocolTransport uses this to write to the underlying transport.
        self.log.write('x-sent: %r\n' % data)
        super(EncryptedTelnetTransport, self).write(self.encrypt(data))

    def writeSequence(self, seq):
        # ProtocolTransport uses this to write to the underlying transport.
        self.log.write('sentSequence\n')
        super(EncryptedTelnetTransport, self).write([
            self.encrypt(data) for data in seq
        ])

    def applicationDataReceived(self, data):
        self.terminal.applicationDataReceived(data)


class TelnetTerminal(object):
    def __init__(self):
        self.telnet_connection = None
        self.terminal = None
        self.buf = []
        self._is_producing = True

    def registerProducer(self, producer, streaming):
        "Register to receive data from a producer."
        self.terminal = producer

    def unregisterProducer(self):
        "Stop consuming data from a producer, without disconnecting."
        self.terminal = None

    def registerTelnet(self, telnet):
        self.telnet_connection = telnet

    def unregisterTelnet(self):
        self.telnet_connection = None

    def write(self, data):
        """Write data to the underlying telnet connection.
        """
        if self.telnet_connection is not None:
            self.telnet_connection.write(data)

    def dataReceived(self, data):
        """
        Called whenever data is received.

        Use this method to translate to a higher-level message.  Usually, some
        callback will be made upon the receipt of each complete protocol
        message.

        Please keep in mind that you will probably need to buffer some data
        as partial (or multiple) protocol messages may be received!  We
        recommend that unit tests for protocols call through to this method
        with differing chunk sizes, down to one byte at a time.

        @param data: bytes of indeterminate length
        @type data: L{bytes}
        """
        self.write(data)

    def applicationDataReceived(self, data):
        if self.terminal is not None:
            self.terminal.write(data)
        else:
            self.buf.append(data)

    def connectionLost(self, reason):
        """Called when the connection is shut down."""
        if self.telnet_connection is not None:
            self.telnet_connection.loseConnection()

    def stopProducing(self):
        self.terminal = None

    def makeConnection(self, transport):
        """Make a connection to a transport and a server."""
        self.terminal = transport
        # register us as a producer on the write port.
        transport.registerProducer(self, True)
        if self.buf:
            self.terminal.write(b''.join(self.buf))

    def connectionMade(self):
        """
        Called when a connection is made.

        This may be considered the initializer of the protocol, because
        it is called when the connection is completed.  For clients,
        this is called once the connection to the server has been
        established; for servers, this is called after an accept() call
        stops blocking and a socket has been received.  If you need to
        send any greeting or initial message, do it here.
        """
